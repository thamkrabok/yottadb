#!/bin/bash

# resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ]; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '/.*' > /dev/null; then
    PRG="$link"
  else
    PRG=`dirname "$PRG"`/"$link"
  fi
done

# Get standard environment variables
PRGDIR=`dirname "$PRG"`

#if [ $# -lt 1 ]
#then
#        echo "Usage: `basename $0` <profile-dir>"
#        exit 1
#fi

export YDB_DIR=/ydbdir

if [ ! -f ${YDB_DIR}/ydbenv_local ]
then
        echo "${YDB_DIR} is invalid YottaDB directory"
        exit 2
fi

. ${YDB_DIR}/ydbenv_local

export export ydb_routines="${PRGDIR} ${ydb_routines}"
HOSTNAME=`hostname -s`
EPOCH=`date +%s`

REPL_STATE=`/ydbdir/monitor/repl_state.sh`
ALIVE=`mupip replicate -source -check 2> /dev/null | awk 'NR==1 {print $8}'`
MODE=`mupip replicate -source -check 2>&1 | grep ACTIVE | awk 'NR==1 {print $8}'`



if [[ ${ALIVE} == "" ]] && [[ ${REPL_STATE} == "ON" ]]
then
        ### Case 1 Replication ON , Source Server Not Alive
        echo "ydb_response_time,host=${HOSTNAME},dir=${YDB_DIR} elap=0,alive=0,mode=0 ${EPOCH}000000000"
        exit 0
fi


if [[ ${MODE} != "ACTIVE" ]] && [[ ${ALIVE} != "" ]] && [[ ${REPL_STATE} == "ON" ]]
then
        ### Case 2 Replication On , Secondary Mode
        echo "ydb_response_time,host=${HOSTNAME},dir=${YDB_DIR} elap=0,alive=1,mode=2 ${EPOCH}000000000"
        exit 0
fi


elap=`timeout 5 mumps -run ZELAP 2> /dev/null`


if [[ ${REPL_STATE} == "OFF" ]] && [[ ! -z ${elap} ]]
then
        ### Case 3 Replication Off , Alive
        echo "ydb_response_time,host=${HOSTNAME},dir=${YDB_DIR} elap=${elap},alive=1,mode=3 ${EPOCH}000000000"
        exit 0

elif [[ ${REPL_STATE} == "OFF" ]] && [[ -z ${elap} ]]
then
        ### Case 4 Replication Off , Not Alive
        echo "ydb_response_time,host=${HOSTNAME},dir=${YDB_DIR} elap=0,alive=0,mode=3 ${EPOCH}000000000"
        exit 0
fi

### Case 5 Replication On , Primary Mode
echo "ydb_response_time,host=${HOSTNAME},dir=${YDB_DIR} elap=${elap},alive=1,mode=1 ${EPOCH}000000000"
