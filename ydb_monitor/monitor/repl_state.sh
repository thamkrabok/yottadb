#!/bin/bash

. /ydbdir/ydbenv_local

reg="DATA"

for region in ${reg}
do

err_file=/tmp/repl_state_${region}_`date +%Y%m%d%H%M%S_%s%N`.err

dse 2>${err_file} 1> /dev/null  <<EOF
        f -r=${region}
        d -f
        exit
EOF

       grep "Replication State" ${err_file} | awk '{print $3}'
       rm -f ${err_file}

done
