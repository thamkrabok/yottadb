# Read Before Going Further

This Repository are for developing the utility shell scripts for YottaDB. To make YottaDB easier to use without deep technical knowledge. You can fork the project and do the pull request when you done.

    Main Repo Page : https://gitlab.com/thammanoon.ph/yottadb
    Newest YottaDB Version : R1.30

# Setup Env in Git Bash
If Git bash initial format is in DOS. You need to change setting in Git Bash format to Unix, Setting step are the following.
```
git config --global core.autocrlf false
```

# Setup Fork Repo
If you new to Git just press the fork button on the main page, link above.
Are if you don't how to update the fork repo just do the following step. ( If there are efficient way you can edit this README.md file and do the pull request)

## Setup Local Repo Upstream to Main Repo
```
# add Main Repo
git remote add upstream https://gitlab.com/thammanoon.ph/yottadb.git
# fetch all branch in main repo
git fetch upstream
# Make sure that you on master branch
git checkout master
# Rebase Local repo to Main Repo
git rebase upstream/master
# Pull any change
git pull
# Update to Fork Repo
git push -u origin master
```

# Table of Content
1. [Installation Step](#1-installation-step)
2. [Documents](#2-documents)

---

# 1. Installation Step 
```
User : root

./install_yottadb.sh
```

# 2. Documents
```
ydb_directoty : Contains YottaDB Instance Directory
ydb_distribute : Contains YottaDB Distribution Binary in Zip
ydb_external : Contains YottaDB External Code in Different Language
ydb_monitor : Contains YottaDB Monitoring Scripts
ydb_utilities_scripts: Contains YottaDB Utilities Shell Scripts
```