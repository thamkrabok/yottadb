# Utilities Scripts Details
## Common Usage
### show_region
```
Type : Execute
Script that get all region name
```
### freeze_state
```
Type : Execute
Script that get database region freeze status
```
## Replication Usage
### repl_state
```

```
### replication_check
```

```
### replcation_on
```

```
### replication_off
```

```
### replication_start
```

```
### replication_stop
```

```
### primary_start
```

```
### primary_stop
```

```
### secondary_start
```

```
### secondary_stop
```

```
### start_multi_site_replication
```

```